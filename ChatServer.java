import java.io.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.*;
import java.util.HashMap;

public class ChatServer implements ChatServer_itf {
  private HashMap<String, ChatRoom_itf> chats;

  public static void main(String[] args) {
    try {
      ChatServer chatServer = new ChatServer();
      ChatServer_itf chatServer_stub =
          (ChatServer_itf)UnicastRemoteObject.exportObject(chatServer, 0);

      Registry registry = LocateRegistry.getRegistry();
      registry.bind("ChatServer", chatServer_stub);

      File historyDir = new File(".history");
      if (!historyDir.exists() && !historyDir.mkdir()) {
        System.err.println("Unable to create .history directory");
        System.exit(1);
      }

      System.out.println("Server ready.");
    } catch (AccessException ae) {
      System.err.println("Access to the local registry has been denied");
      ae.printStackTrace();
      System.exit(1);
    } catch (AlreadyBoundException abe) {
      System.err.println("A chat server has already been started.");
      abe.printStackTrace();
      System.exit(1);
    } catch (RemoteException re) {
      System.err.println("Either the server stub could not be exported or the"
                         + "registry not be locatd.");
      System.err.println("Most probably the registry has not been started.");
      re.printStackTrace();
      System.exit(1);
    } catch (Exception e) {
      System.err.println("Unexpected exception: " + e);
      e.printStackTrace();
      System.exit(1);
    }
  }

  ChatServer() { this.chats = new HashMap<String, ChatRoom_itf>(); }

  /* Map chat room names to the appropriate chat room stubs.
   * This method is synchronized so that a chat room name only ever maps to one
   * stub.
   */
  public synchronized ChatRoom_itf getChatRoom(String name)
      throws RemoteException {
    ChatRoom_itf chatRoom = this.chats.get(name);

    if (chatRoom != null) {
      return chatRoom;
    } else {
      chatRoom = new ChatRoom(name);
      ChatRoom_itf chatRoom_stub =
          (ChatRoom_itf)UnicastRemoteObject.exportObject(chatRoom, 0);
      this.chats.put(name, chatRoom_stub);
      return chatRoom_stub;
    }
  }
}
