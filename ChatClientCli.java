import java.io.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

class ChatClientCli implements ChatClient_itf {

  private ChatServer_itf chatServer;
  private ChatRoom_itf currentChat;
  private UUID currentUUID;
  private String currentUsername;
  private ChatClient_itf stub;

  public static void main(String[] args) {
    if (args.length != 1) {
      System.err.println("usage: java -jar chat-app-client.jar <host>");
      System.exit(1);
    }

    String host = args[0];
    try {
      Registry registry = LocateRegistry.getRegistry(host);
      ChatServer_itf server = (ChatServer_itf)registry.lookup("ChatServer");

      ChatClientCli client = new ChatClientCli(server);
      client.userInteraction();
    } catch (AccessException ae) {
      System.err.println("Acess to the local registry has been denied.");
      ae.printStackTrace();
      System.exit(1);
    } catch (RemoteException re) {
      System.err.println("Communication with the registy failed. Make sure you"
                         + " started the client with the correct address.");
      re.printStackTrace();
      System.exit(1);
    } catch (NotBoundException nbe) {
      System.err.println("The chat server has not been started/registered.");
      nbe.printStackTrace();
      System.exit(1);
    }
  }

  ChatClientCli(ChatServer_itf chatServer) throws RemoteException {
    this.chatServer = chatServer;
  }

  private void userInteraction() {
    try {
      while (true) {
        System.out.print("Enter name of chatroom to enter: ");
        BufferedReader stdIn =
            new BufferedReader(new InputStreamReader(System.in));
        String chatName = stdIn.readLine().trim();

        ChatRoom_itf chatRoom = this.chatServer.getChatRoom(chatName);

        UUID new_uuid = chooseName(stdIn, chatRoom);
        assert (new_uuid != null);

        this.currentUUID = new_uuid;
        this.currentChat = chatRoom;

        System.out.println("Enter message and send with <CR>.");

        printHistory(getChatHistory(this.currentChat));
        chattingLoop();
      }
    } catch (RemoteException re) {
      System.err.println("The connection to the server/chat room has failed."
                         + " Please try again later.");
      // When we encounter a RemoteException deregistering the client is
      // hopeless
      re.printStackTrace();
    } catch (IOException ioe) {
      // System.out calls or chatRoom or chattingLoop
      deregisterAndPrintException(ioe);
    } catch (NullPointerException npe) {
      // Happens when the user CTRL-D's the interaction
      try {
        this.currentChat.deregister(this.currentUUID);
        // So we do not expose the NullPointerException to the user
        System.exit(0);
      } catch (RemoteException re) {
        // just give up on a clean exit
        re.printStackTrace();
        System.exit(1);
      }
    } catch (Exception e) {
      e.printStackTrace();
      System.exit(1);
    }
  }

  private void deregisterAndPrintException(Exception e) {
    try {
      this.currentChat.deregister(this.currentUUID);
      e.printStackTrace();
    } catch (RemoteException re) {
      // just give up on a clean exit
      re.printStackTrace();
    }
  }

  private UUID chooseName(BufferedReader user, ChatRoom_itf chatRoom)
      throws RemoteException, IOException {
    boolean name_accepted = false;
    ChatClient_itf stub = getStub();
    UUID new_uuid = null;

    // RemoteException and IOException are handled in main
    while (!name_accepted) {
      System.out.print("Please enter a username: ");
      String requested_name = user.readLine().trim();
      new_uuid = chatRoom.register(stub, requested_name);

      if (new_uuid != null) {
        this.currentUsername = requested_name;
        break;
      }
      System.out.println("Username refused. Please try again.");
    }

    return new_uuid;
  }

  private ChatClient_itf getStub() throws RemoteException {
    if (this.stub == null) {
      ChatClient_itf stub =
          (ChatClient_itf)UnicastRemoteObject.exportObject(this, 0);
      this.stub = stub;
      return stub;
    } else {
      return this.stub;
    }
  }

  private List<ChatMessage> getChatHistory(ChatRoom_itf chatRoom)
      throws RemoteException {
    return chatRoom.getHistory();
  }

  private void printHistory(List<ChatMessage> history) {
    history.forEach(message -> printMessage(message));
  }

  public void receive(ChatMessage message) throws RemoteException {
    printMessage(message);
  }

  public void send(ChatRoom_itf room, String message) throws RemoteException {
    room.broadcast(this.currentUUID, message);
  }

  private void chattingLoop() throws IOException {
    BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
    String userInput = null;

  chat_loop:
    while ((userInput = stdIn.readLine().trim()) != null) {
      switch (userInput) {
      case "/help":
        System.out.println("Type \"leave\" to leave the chat room.");
        System.out.println("Type \"help\" to see this message.");
        break;
      case "/leave":
        this.currentChat.deregister(this.currentUUID);
        break chat_loop;
      default:
        System.out.print("\033[1A"); // Move up
        System.out.print("\033[2K"); // Erase line content
        send(this.currentChat, userInput);
        break;
      }
    }
  }

  private void printMessage(ChatMessage message) {
    Timestamp t = new Timestamp(message.getTime());
    LocalDateTime ldt = t.toLocalDateTime();
    /* The Java documentation does not seem to guarantee System.out to be thread
     * safe. Since `receive` is part of the remote interface this prepares for
     * the case of concurrent calls to receive and thus printing. Right now this
     * situation should not be possible since the client is only connected to
     * one chat room. */
    synchronized (System.out) {
      System.out.println("<" + ldt.toString() + "> " + message.getUser() +
                         ": " + message.getBody());
    }
  }
}
