# Java RMI Chat Application

## How to build

- `make jar_client`
- `make jar_server`

## How to run
### Server
#### Using Docker
```sh
docker build -t <img name> .
docker run -d -p 1099:1099 <img name>
```
This starts the chat server on the host running docker, mapping the host's port
1099 to the docker container's port 1099.

#### Manual

For your convenience, you can simply run `make start_server` which will start
both the server and the RMI registry.

Otherwise, type `java -jar chat-app-server.jar` to run a chat client instance in
your terminal. Make sure to have an RMI registry running with the right
CLASSPATH environment variable set (this directory).

### Client
Run `java -jar chat-app-client.jar localhost` in your terminal to connect to a
chat server on the same host. We have not dockerized the client since we
highly doubt a user is going to run

## Organization of the Project
This simple chat application is roughly organized in two parts. These
correspond to the basic entities in this distributed system:
1. A chat server which spawns chat rooms and mediates between users and chat
   rooms (service discovery).
2. Clients which connect to a server to exchange messages.

### Clients
We have implemented a command line interface (CLI) client to interact with the
server, chat rooms and other clients.
After successful connection to the chat server the client will ask the user to
join a chat room and pick a username. Afterwards the user can post messages to
the chat room. A chat room can be left with the `/leave` command upon which the
user can join a different chat room. `/help` will guide the user.
Press `Ctrl+D` to respectfully exit the client.

### Server
There is a central server that is registered in the RMI registry on startup.
Clients connect to the server and can ask it to join a chat room. If there
already exists a chat room by this name, the user will receive the appropriate
RMI chat room stub for further interaction.
Otherwise, a new chat room instance is spun up and a new RMI chat room stub
returned. Subsequent join requests for the same name will be directed to this
instance.

#### Chat Rooms
Chat rooms perform the heavy lifting in this system. They handle:
* The de/-registration of clients (inclusive username validation)
* The distribution of messages
* Keeping track of the history of messages

## Technical Notes
* In RMI terminology the chat server, chat rooms and chat clients all represent
  RMI servers. However, only the chat server has to be registered in the RMI
  registry. The discovery of chat rooms by clients is handled by the chat
  server.

* Usernames are cached on the chat room side. A previous design used a getter
  method for the username exposed by the client stub. This approach was
  rejected because it incurs unnecessary network calls. Furthermore, name
  changes have not been implemented and should not be at the discretion of the
  client/user.

* We implemented our own ad-hoc de-/serialization code.
  - Java's de-/serialization for objects can only write one object per file.
    This means that our potentially long list of messages has to be serialized
    in ins entirety for every history update instead of being able to simply
    append to the file.
   - Our de-/serialization code can deal with changes in the definition of chat
     messages via primitive schema versions. This feature has already been
     useful once during the development.

* Dockerization of the server code
  - To simplify the DevOps experience of running our chat server, we have
    dockerized the server side of our system.
  - Importantly, this dockerization simplifies the integration into a
    continuous integration environment. Currently it is only tested whether the
    checked-in code compiles. However, now it is trivial to add unit and
    integration testing to the pipeline. Pipeline can be seen by following this
    [link](https://gitlab.ensimag.fr/kessibig/chat-app/pipelines)

## Possible Roadmap
* Destroy empty chat rooms
* Use a professional de-/serialization library
* User-visible features (see any popular messenger for inspiration)
