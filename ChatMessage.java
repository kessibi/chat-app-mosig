import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* This class represents a chat message in our system.
 * Due to issues with the built-in Java serialization it implements its own
 * custom serialization based on text and regular expressions. We opted for
 * this approach instead of requiring a non-standard-library library like gson
 * or Jackson
 */
class ChatMessage implements Serializable {
  private String username;
  private String message;
  private long timestamp; // time in milliseconds since unix epoch

  ChatMessage(String username, String msg, long ts) {
    this.username = username;
    this.message = msg;
    this.timestamp = ts;
  }

  /* The deserialization first checks for the used schema and proceeds
   * accordingly.
   * The current format of the serialization function and the latest schema
   * should match.
   */
  static ChatMessage deserialize(String serializedMessage)
      throws IllegalArgumentException {
    try {
      Pattern getVer = Pattern.compile("schemaV(\\d\\d\\d)(.*)");
      Matcher verMatch = getVer.matcher(serializedMessage);
      verMatch.matches();

      int schemaVersion = Integer.parseInt(verMatch.group(1));
      String rest = verMatch.group(2);

      Pattern getMessage;
      Matcher messageMatch;
      String millisString, username, content;
      long millis;

      switch (schemaVersion) {
      case 1:
        // schemaV001 uses whitespace as separators
        getMessage = Pattern.compile("\\s+(\\d+)\\s+(\\w+)\\s+(.*)");
        messageMatch = getMessage.matcher(rest);
        messageMatch.matches();

        millisString = messageMatch.group(1);
        millis = Long.parseLong(millisString); // might throw
        username = messageMatch.group(2);
        content = messageMatch.group(3);

        return new ChatMessage(username, content, millis);
      case 2:
        /* schemaV002 improves upon schemaV001 in one major aspect. Usernames
         * are now saved with surrounding quotes. This allows more complex
         * usernames, in particular ones with whitespace characters inside them
         */
        getMessage = Pattern.compile("\\s+(\\d+)\\s+\"(.+?)\"\\s+(.*)");
        messageMatch = getMessage.matcher(rest);
        messageMatch.matches();

        millisString = messageMatch.group(1);
        millis = Long.parseLong(millisString); // might throw
        username = messageMatch.group(2);
        content = messageMatch.group(3);

        return new ChatMessage(username, content, millis);
      default:
        throw new IllegalArgumentException(
            "Unknown schema version for ChatMessage "
            + "string serialization.");
      }
    } catch (NumberFormatException nfe) {
      throw new IllegalStateException("History deserialization: Supposed schema"
                                      + "version is not a number.");
    } catch (IllegalStateException ise) {
      throw new IllegalArgumentException(
          "History deserialization: Regex"
          + "pattern match failed on history entry.");
    } catch (IndexOutOfBoundsException ioobe) {
      throw new IllegalArgumentException("History deserialization: Pattern"
                                         + "match tried to acces non-existing"
                                         + "matching group");
    }
  }

  public String getBody() { return this.message; }
  public String getUser() { return this.username; }
  public long getTime() { return this.timestamp; }

  String serialize() {
    String sep = " ";
    return ("schemaV002" + sep + this.timestamp + sep + "\"" + this.username +
            "\"" + sep + this.message);
  }
}
