#!/bin/sh
# start the registry
rmiregistry&

# mandatory for the RMI registry to setup
sleep 1

# starting the chat server
java -jar chat-app-server.jar

