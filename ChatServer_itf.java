import java.rmi.*;
import java.util.UUID;

interface ChatServer_itf extends Remote {
  public ChatRoom_itf getChatRoom(String name) throws RemoteException;
}
