import java.rmi.*;
import java.util.List;
import java.util.UUID;

/* Spawn a new instance of this class per chat room.
 * One server -> multiple chat rooms
 * TODO: history retrieval based on client scrolling, nice to have
 */
interface ChatRoom_itf extends Remote {
  /*
   * - Internally keeps a list of registered users which will receive messages
   *   sent to the server.
   * - Could instead return some kind of handle/token to be send by the client
   *   for further interactions. The server would then keep a map from tokens
   *   to ChatClient_itf.
   *   Most likely an optimization that isn't worth it right now.
   */
  public UUID register(ChatClient_itf client, String requestedName)
      throws RemoteException;

  /* Deregister from (leave) chat room.
   * The user must identify with its UUID, otherwise it would be way too easy
   * to maliciously deregister other clients. */
  public void deregister(UUID uid) throws RemoteException;

  /*
   * - Receive message from user. The received message should be forwarded to
   *   all registered clients. User is identified with its UUID.
   */
  public void broadcast(UUID userId, String message) throws RemoteException;

  /*
   * - get History list from the chat room
   */
  public List<ChatMessage> getHistory() throws RemoteException;
}
