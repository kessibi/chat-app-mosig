import java.io.*;
import java.rmi.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/*
 * Main implementation of our ChatRoom_itf
 */
class ChatRoom implements ChatRoom_itf {
  private class UserRecord {
    String username;
    ChatClient_itf cstub;

    UserRecord(String user, ChatClient_itf stub) {
      this.username = user;
      this.cstub = stub;
    }
  }

  private Map<UUID, UserRecord> clients;
  private List<ChatMessage> history;
  private String chatName;
  private UUID systemClient;
  private int historyCheckpoint;

  ChatRoom(String name) {
    this.chatName = name;
    // clients is a synchronizedMap to avoid concurrency problems on read/write
    this.clients = Collections.synchronizedMap(new HashMap<UUID, UserRecord>());
    this.historyCheckpoint = 0;
    this.systemClient = UUID.randomUUID();

    try {
      // all log files are saved in a .history directory
      BufferedReader in =
          new BufferedReader(new FileReader(".history/log_" + this.chatName));
      String s;

      // retrieve history from the corresponding file
      this.history = Collections.synchronizedList(new ArrayList<ChatMessage>());
      while ((s = in.readLine()) != null) {
        this.history.add(ChatMessage.deserialize(s));
      }
      this.historyCheckpoint = this.history.size();

      in.close();
    } catch (IllegalArgumentException iae) {
      // History deserialization failed
      // Notify server log of this failure and start with a fresh history
      System.err.println(iae.getMessage());
      this.history = new ArrayList<ChatMessage>();
    } catch (IOException e) {
      this.history = new ArrayList<ChatMessage>();
    }
  }

  public UUID register(ChatClient_itf client, String requested_username)
      throws RemoteException {
    // Validate on server side to prevent abuse
    if (!isValidUsername(requested_username)) {
      return null;
    }

    // new ChatMessage warning of the user leaving
    ChatMessage c = new ChatMessage(
        "SYSTEM", requested_username + " has joined the chat room",
        new Date().getTime());
    this.history.add(c);

    // record to history as it is an important event
    this.saveHistory();
    this.historyCheckpoint = this.history.size();

    this.clients.forEach((uid, record) -> {
      ChatClient_itf cstub = record.cstub;
      assert (cstub != null); // otherwise hash maps desync'd
      try {
        // send the message to each client
        cstub.receive(c);
      } catch (RemoteException e) {
        e.printStackTrace();
      }
    });

    // add new user to the list
    UUID new_uuid = UUID.randomUUID();
    this.clients.put(new_uuid, new UserRecord(requested_username, client));
    return new_uuid;
  }

  /* Send message to all currently registered clients in the chat room.
   * Because we do not trust the user's clock, the server inserts the time
   * stamp. For this reason the message is also sent back to the original
   * sender.
   */
  public void broadcast(UUID userId, String message) throws RemoteException {
    UserRecord user = this.clients.get(userId);

    if (user == null) {
      return;
    }

    ChatMessage c =
        new ChatMessage(user.username, message, new Date().getTime());
    this.history.add(c);
    if (this.history.size() - this.historyCheckpoint >= 10) {
      this.saveHistory();
      this.historyCheckpoint = this.history.size();
    }

    ArrayList<UUID> to_unregister = new ArrayList<UUID>();

    // send message to each client
    this.clients.forEach((uid, record) -> {
      ChatClient_itf cstub = record.cstub;
      assert (cstub != null); // otherwise hash maps desync'd

      try {
        cstub.receive(c);
      } catch (RemoteException _e) {
        System.err.println("User " + record.username +
                           " doesn't exist anymore");
        to_unregister.add(uid);
      }
    });

    // unregister users that were not reachable
    to_unregister.forEach((uid) -> {
      try {
        this.deregister(uid);
      } catch (RemoteException e) {
        System.err.println("RemoteException: " + e);
        e.printStackTrace();
      }
    });
  }

  public void deregister(UUID userId) throws RemoteException {
    UserRecord user = this.clients.get(userId);
    if (user == null) {
      /* Ignore unregister if the uuid is not registered */
      return;
    }
    String userLeaving = user.username;
    this.clients.remove(userId);

    // new ChatMessage warning of the user leaving
    ChatMessage c =
        new ChatMessage("SYSTEM", userLeaving + " has left the chat room",
                        new Date().getTime());
    this.history.add(c);

    // record to history as it is an important event
    this.saveHistory();
    this.historyCheckpoint = this.history.size();

    // send unregister message to the other users
    this.clients.forEach((uid, record) -> {
      ChatClient_itf cstub = record.cstub;
      assert (cstub != null); // otherwise hash maps desync'd
      try {
        cstub.receive(c);
      } catch (RemoteException e) {
        e.printStackTrace();
      }
    });

    System.out.println("Removing client " + user.username + " from the list.");
  }

  public List<ChatMessage> getHistory() throws RemoteException {
    return this.history;
  }

  private boolean isValidUsername(String username) {
    // The `"` character is used in schemaV002 (see ChatMessage) of our ad-hoc
    // message serialization
    if (username.contains("\"")) {
      return false;
    }

    // Special case user, SYSTEM
    // This user is used to send messages by the system to the users e.g.
    // join or leave announcements of users.
    // Could also be used to warn about maintenance windows etc.
    if (username.equals("SYSTEM")) {
      return false;
    }

    // Does the name match another user's name
    for (UserRecord user : this.clients.values()) {
      if (user.username.equals(username))
        return false;
    }

    return true;
  }

  /*
   * for each new message that has not been appended, append it to corresponding
   * history file
   */
  private void saveHistory() {
    try {
      BufferedWriter out = new BufferedWriter(
          new FileWriter(".history/log_" + this.chatName, true));

      int size = this.history.size();
      for (int i = this.historyCheckpoint; i < size; i++) {
        out.write(this.history.get(i).serialize());
        out.newLine();
      }
      
      out.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
