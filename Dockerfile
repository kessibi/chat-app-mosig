FROM odclive/jdk-make:latest AS build-env
# image with openjdk and the make cmd

WORKDIR /tmp/chat-build

ADD . /tmp/chat-build
# copy source files to work dir

RUN make jar_server
# create the server jar

FROM openjdk:15-jdk-alpine

WORKDIR /root/chat-app

COPY --from=build-env /tmp/chat-build/chat-app-server.jar chat-app-server.jar
# copy the server from the build environment to the real image

COPY resources/start_server.sh .

RUN chmod +x start_server.sh

ENV CLASSPATH /root/chat-app/chat-app-server.jar
# setup CLASSPATH for RMI to find the classes

EXPOSE 1099
# base RMI port for outside communication

CMD ["./start_server.sh"]
