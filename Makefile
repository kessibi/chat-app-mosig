jar_server=chat-app-server.jar
running_rmi := $(shell pgrep rmiregistry > /dev/null ; echo $$?)

all:
	javac *.java

clean:
	rm -f *.class *.jar

remove_history:
	rm -rf .history

jar_client: all
	jar cfm chat-app-client.jar resources/Manif_client ChatClientCli.class \
		ChatClient_itf.class ChatMessage.class ChatRoom_itf.class \
		ChatServer_itf.class 

jar_server: all
	jar cfm chat-app-server.jar resources/Manif_server ChatClient_itf.class \
		ChatMessage.class 'ChatRoom$$UserRecord.class' ChatRoom.class \
		ChatRoom_itf.class ChatServer.class ChatServer_itf.class

start_server: jar_server
ifeq ($(running_rmi), 0)
	kill $$(pgrep rmiregistry)
	sleep 1
endif
	CLASSPATH="${PWD}/$(jar_server)" rmiregistry&
	sleep 1
	java -jar chat-app-server.jar
