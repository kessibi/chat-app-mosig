import java.rmi.*;

interface ChatClient_itf extends Remote {
  /* Receive message from chat room server.
   * Change state of instance appropriately e.g. by displaying the new message.
   */
  public void receive(ChatMessage message) throws RemoteException;

  /* Send message to server.
   * - Method returns after message has been sent to all other registered
   * clients of the chat room ? Does not receive own message. ? Throws an error
   * if the client is not registered with the chat server.
   */
  public void send(ChatRoom_itf room, String message)
      throws RemoteException;
}
